public class CreneauHoraire {
//    Développer une classe CreneauHoraire caractérisée par un numéro de jour, d’ heure et de minute de début, ainsi que par une durée en minutes.
    public int numJour;
    public int heureDebut;
    public int minuteDebut;
    public int duree;
    public CreneauHoraire(int numJour, int heureDebut, int minuteDebut, int duree) {
        this.numJour = numJour;
        this.heureDebut = heureDebut;
        this.minuteDebut = minuteDebut;
        this.duree = duree;
    }
}
